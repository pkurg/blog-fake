<?php namespace Pkurg\BlogFakeData\Controllers;

use Backend\Classes\Controller;
use Carbon\Carbon;
use Pkurg\BlogFakeData\Models\Settings;
use RainLab\Blog\Models\Category;
use RainLab\Blog\Models\Post;
use System\Models\File;
use Session;


class GenFakeData extends Controller
{
	public $implement = [];

	public function __construct()
	{
		parent::__construct();
	}

	public function progress()
	{

		return  \Response::json( Session::get('fakeblogdata'));;
	}

	public function index()
	{




		if (class_exists(Post::class)) {

			$factory = \Faker\Factory::create();			

            //Categories generate

			for ($i = 0; $i < Settings::get('category_count'); $i++) {

				\RainLab\Blog\Models\Category::create([
					'name' => $factory->company,
					'slug' => $factory->uuid,
				]);

			//	Session::put('fakeblogdata', $i);

			}

            //Posts generate

			$AllCategories = Category::pluck('id')->toArray();

			for ($i = 0; $i < Settings::get('post_count'); $i++) {

				$post = new Post();
				$post->title = $factory->realText(20);
				$post->slug = $factory->slug();
				$post->content = $factory->realText(450);
				$post->excerpt = $factory->realText(120);
				$post->published_at = Carbon::now();
				$post->published = true;
				$post->save();

				$post->categories()->attach($AllCategories[array_rand($AllCategories)]);


				

				if (Settings::get('genimage')) {
					$file = new File;
					$file->fromUrl('https://picsum.photos/500/320/?random=1',$factory->slug() . '.jpg');        
					
					$post->featured_images()->save($file);
				}

				

				//Session::put('fakeblogdata', $i);

				
			}
		}

		return true;
	}

}
