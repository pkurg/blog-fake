<?php namespace Pkurg\BlogFakeData;

use Pkurg\BlogFakeData\Models\Settings;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;

class Plugin extends PluginBase
{
	
	public $require = ['RainLab.Blog'];

	public function registerComponents()
	{
	}

	public function registerSettings()
	{

		return [
			'settings' => [
				'label' => 'Blog Fake Data',
				'description' => 'Generate Blog Fake Data',
				'category' => SettingsManager::CATEGORY_CMS,
				'icon' => 'oc-icon-database',
				'class' => 'Pkurg\BlogFakeData\Models\Settings',
				'order' => 500,
                //'permissions' => ['pkurg.blogfakedata.manage'],

			],
		];
	}

	public function boot()
	{

		if (Settings::get('post_count') == '') {
			Settings::set('post_count', 15);
		}
		if (Settings::get('category_count') == '') {
			Settings::set('category_count', 5);
		}

	}

}
