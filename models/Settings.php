<?php

namespace Pkurg\BlogFakeData\Models;

use Model;

class Settings extends Model
{

    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'blog_fake_data_settings';

    public $settingsFields = 'fields.yaml';

    protected $cache = [];



}
